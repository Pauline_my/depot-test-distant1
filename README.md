# Mon premier dépôt Git Ceci est mon premier dépôt.

## Liste des commandes
- `git init` : initialise le dépôt
- `git add` : ajoute un fichier à la zone d'index
- `git commit` : valide les modifications indexées dans la zone d'index
- `git log` : affiche l'historique des commit 
- `git status` : vérifie les fichiers modifiés ou non et les fichiers non ajoutés
- `git push mon_depot_distant master` : permet d'envoyer à mon projet Gitlab la version de mes fichiers
- `git remote add mon_depot_distant git@gitlab.com:[votre login]/[slug du dépôt]` : pour ajouter le dépôt GitLab comme remote avec le nom "mon_depot_distant"
- `git branch master --set-upstream-to=mon_depot_distant/master` : permet d'utiliser `git push` sans mettre la suite. (à faire pour chaque projet)
- ls ~\.ssh permet de chercher les fichiers .ssh dans l'ordinateur (marche avec d'autres .xx)
- `rm -fr .git` permet de supprimer un repertoire git.
- `git pull`: permet de prendre les fichiers de mon dépôt 
- `gedit .gitignore` : mettre le nom des fichiers ignorés
- `git clone [lien SSH ou HTTPS]` : permet de cloner un dépôt distant en local
- `git remote set-url [url]` : pour changer l'url du remote 
- `git tag [nom] [SHA du commit]` :  met une étiquette à un commit, pour push `git push --tags` 

